#include<stdio.h>
#include<math.h>
struct point
{
   float x;
   float y;
};
typedef struct point Point;
Point input()
{
  Point p;
  printf("Enter the value of abcissa");
  scanf("%f",&p.x);
  printf("Enter the value ofordinate");
  scanf("%f",&p.y);
  return p;
}

float compute(Point p1,Point p2)
{
  float distance;
  distance=sqrt(pow((p1.x-p2.x),2)+pow((p1.y-p2.y),2));
  return distance;
}

float output(Point p1,Point p2,float distance)
{
  printf("the distance between %f,%f and %f,%f is %f",p1.x,p1.y,p2.x,p2.y,distance);
}

int main(void)
{
  float distance;
  Point p1,p2;
  p1=input();
  p2=input();
  distance=compute(p1,p2);
  output(p1,p2,distance);
  return 0;
}
