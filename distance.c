#include<stdio.h>
#include<math.h>
float input(char a,int b)
{
  float n;
  printf("enter the value of the coordinate %c%d\n",a,b);
  scanf("%f",&n);
  return n;
}
 
 float distance(float x1,float y1,float x2,float y2)
{
   float distance=sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)));
   return distance;
}
 void output(float x1,float y1,float x2,float y2,float d)
{
 printf("the distance between (x1,y1)=%f,%f and (x2,y2)=%f,%f is %f\n",x1,y1,x2,y2,d);
}

int main()
{
   float x1,x2,y1,y2,d;
   x1=input('x',1);
   y1=input('y',1);
   x2=input('x',2);
   y2=input('y',2);
   d=distance(x1,y1,x2,y2);
   output(x1,y1,x2,y2,d);
   return 0;
}
