#include<stdio.h>
float input(char a)
{
  float n;
  printf("Enter the value of %c\n",a);
  scanf("%f",&n);
  return n;
}

float volume(float h,float d,float b)
{
   float volume= ((h*d*b)/3)+((d/b)/3);
   return volume;
}

void output(float h,float d,float b,float v)
{
   printf("the volume of h=%f,d=%f,b=%f is %f",h,d,b,v);
}

int main()
{
   float h,d,b,v;
    h=input('h');
    d=input('d');
    b=input('b');
    v=volume(h,d,b);
    output(h,d,b,v);
    return 0;
}
